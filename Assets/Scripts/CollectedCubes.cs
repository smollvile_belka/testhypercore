﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CollectedCubes : MonoBehaviour
{

    [SerializeField]
    private GameObject mainCube;

    private float lenghtRay = .4f;

    
    void FixedUpdate()
    {
        raycastForward();
        raycastRight();
        raycastForwardBack();
        raycastLeft();
        raycastBottom();
    }

    
    public void Rotate()
    {
        if(transform.localEulerAngles.y <=25 || transform.localEulerAngles.y >= 335) 
            transform.DORotate(new Vector3(0,0,0), .1f);

        if(transform.localEulerAngles.y <= 115 && transform.localEulerAngles.y >= 65) 
            transform.DORotate(new Vector3(0,90,0), .1f);

        if(transform.localEulerAngles.y <= 205 && transform.localEulerAngles.y >= 155) 
            transform.DORotate(new Vector3(0,180,0), .1f);
            
        if(transform.localEulerAngles.y <= 295 && transform.localEulerAngles.y >= 245) 
            transform.DORotate(new Vector3(0,-90,0), .1f);
    }

    private void raycastBottom()
    {
        RaycastHit hit;
        Ray ray = new Ray(transform.position, -transform.up);

        Physics.Raycast(ray, out hit, 0.6f);

        if(hit.collider != null)
            if(hit.collider.gameObject.tag == "Lava")
            {
                StartCoroutine(CubeDelete());
            }
    }

    private IEnumerator CubeDelete() 
    {
        yield return new WaitForSeconds(.25f);

        RaycastHit hit;
        Ray ray = new Ray(transform.position, -transform.up);

        Physics.Raycast(ray, out hit, 0.6f);

        if(hit.collider != null)
            if(hit.collider.gameObject.tag == "Lava")
            {
                transform.parent = null;
                transform.gameObject.SetActive(false);
            }

    }

    private void raycastForward()
    {
        RaycastHit hit;
        Ray ray = new Ray(transform.position, transform.forward);

        Physics.Raycast(ray, out hit, lenghtRay);

        if(hit.collider != null)
            if(hit.collider.gameObject.tag == "Wall")
            {
                transform.parent = null;
                
                gameObject.GetComponent<CollectedCubes>().enabled = false;
            }
    }

    private void raycastRight()
    {
        RaycastHit hit;
        Ray ray = new Ray(transform.position, transform.right);

        Physics.Raycast(ray, out hit, lenghtRay);

        if(hit.collider != null)
            if(hit.collider.gameObject.tag == "Wall")
            {
                transform.parent = null;
                
                gameObject.GetComponent<CollectedCubes>().enabled = false;
            }
    }

    private void raycastForwardBack()
    {
        RaycastHit hit;
        Ray ray = new Ray(transform.position, -transform.forward);

        Physics.Raycast(ray, out hit, lenghtRay);

        if(hit.collider != null)
            if(hit.collider.gameObject.tag == "Wall")
            {
                transform.parent = null;
                
                gameObject.GetComponent<CollectedCubes>().enabled = false;
            }
    }

    private void raycastLeft()
    {
        RaycastHit hit;
        Ray ray = new Ray(transform.position, -transform.right);

        Physics.Raycast(ray, out hit, lenghtRay);

        if(hit.collider != null)
            if(hit.collider.gameObject.tag == "Wall")
            {
                transform.parent = null;
                
                gameObject.GetComponent<CollectedCubes>().enabled = false;
            }
    }
}
