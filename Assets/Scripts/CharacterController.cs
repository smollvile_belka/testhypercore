﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CharacterController : MonoBehaviour
{
    
    [Header("Point")]
    [SerializeField]
    private List<Point> point;

    [Header("Rotate")]
    [SerializeField]
    private float speedRotateCube = .05f;

    private bool isMove = false;

    private Transform plane;

    private int counterRatate = 0;

    private float boundaries;
    
    private Vector3[] pointPath;

    private Tween pathRotate;
    private Tween move;


    void Awake()
    {
        pointPath = new Vector3[point[counterRatate].getLenghtPoint()];

        for(int i = 0; i < pointPath.Length; i++)
        {
            pointPath[i] = point[counterRatate].getTransform(i).position;
            
            if(i == point[counterRatate].getLenghtPoint()-1) counterRatate++;
        }
    }

    private void Update()
    {
        raycastForward();
        raycastBottom();

        if(isMove)
        {
            moveCharacter();
        }
    }

    public void moveCharacter()
    {

        if(transform.forward == Vector3.forward)
            move = GetComponent<Rigidbody>().DOMoveZ(transform.position.z + 1f, 0.5f);

        if(transform.forward == Vector3.right)
            move = GetComponent<Rigidbody>().DOMoveX(transform.position.x + 1f, 0.5f);

        if(transform.forward == Vector3.left)
            move = GetComponent<Rigidbody>().DOMoveX(transform.position.x - 1f, 0.5f);

        if(transform.forward == -Vector3.forward)
            move = GetComponent<Rigidbody>().DOMoveZ(transform.position.z - 1f, 0.5f);

        isMove = true;
    }

    public void moveHorizontalCharacter(float horizontal)
    {
        
        if(transform.forward == Vector3.forward || transform.forward == -Vector3.forward)
            boundaries = plane.position.x;

        if(transform.forward == Vector3.right || transform.forward == Vector3.left)
            boundaries = plane.position.z;

        float positionX = transform.position.x;
        float positionZ = transform.position.z;

        if(horizontal != 0)
        {
            if(transform.forward == Vector3.forward)
                if((positionX + horizontal) <= (boundaries + 2f) && (positionX + horizontal) >= (boundaries - 2f))
                    transform.position += new Vector3(horizontal, 0, 0);

            if(transform.forward == Vector3.right)
                if((positionZ + (-horizontal)) <= (boundaries + 2f) && (positionZ + (-horizontal)) >= (boundaries - 2f))
                    transform.position += new Vector3(0, 0, -horizontal);

            if(transform.forward == Vector3.left)
                if((positionZ + horizontal) <= (boundaries + 2f) && (positionZ + horizontal) >= (boundaries - 2f))
                    transform.position += new Vector3(0, 0, horizontal);

            if(transform.forward == -Vector3.forward)
                if((positionX + (-horizontal)) <= (boundaries + 2f) && (positionX + (-horizontal)) >= (boundaries - 2f))
                    transform.position += new Vector3(-horizontal, 0, 0);
        } 

    }

    private void raycastForward()
    {
        RaycastHit hit;
        Ray ray = new Ray(transform.position, transform.forward);

        Physics.Raycast(ray, out hit, .7f);

        if(hit.collider != null)
            if(hit.collider.gameObject.tag == "Wall")
            {
                isMove = false;
                move.Kill();
            }
    }

    private void raycastBottom()
    {
        RaycastHit hit;
        Ray ray = new Ray(transform.position, -transform.up);

        Physics.Raycast(ray, out hit, 0.5f);

        if(hit.collider != null)
            if(hit.collider.gameObject.tag == "Lava")
            {
                isMove = false;
                move.Kill();
            }
    }
    
    private void OnTriggerStay(Collider other) 
    {

        if(other.gameObject.tag == "Cube")
        {
            
            other.gameObject.GetComponent<CollectedCubes>().enabled = true;
            other.gameObject.GetComponent<BoxCollider>().isTrigger = false;
            other.gameObject.GetComponent<BoxCollider>().enabled = false;

            float y = transform.position.y + 1f;
            
            transform.position = new Vector3(other.gameObject.transform.position.x, y, other.gameObject.transform.position.z);
            other.gameObject.transform.parent = transform;

            other.gameObject.transform.position = new Vector3(transform.position.x, transform.position.y-transform.childCount, transform.position.z);
            
            other.gameObject.GetComponent<CollectedCubes>().Rotate();
            other.gameObject.GetComponent<BoxCollider>().enabled = true;
        }
    }


    private void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.tag == "Rotate") moveRotate();

        if(other.collider != null && other.gameObject.tag != "Cube")
        {
            plane = other.gameObject.transform;
        }
    }

    private void OnCollisionExit(Collision other)
    {
        if(other.gameObject.tag == "Rotate") 
        {
            pathRotate.Pause();

            if(transform.localEulerAngles.y <=25 || transform.localEulerAngles.y >= 335) 
                transform.DORotate(new Vector3(0,0,0), speedRotateCube);

            if(transform.localEulerAngles.y <= 115 && transform.localEulerAngles.y >= 65) 
                transform.DORotate(new Vector3(0,90,0), speedRotateCube);

            if(transform.localEulerAngles.y <= 205 && transform.localEulerAngles.y >= 155) 
                transform.DORotate(new Vector3(0,180,0), speedRotateCube);
                
            if(transform.localEulerAngles.y <= 295 && transform.localEulerAngles.y >= 245) 
                transform.DORotate(new Vector3(0,-90,0), speedRotateCube);
            

            if(counterRatate < point.Count)
            {
                pointPath = new Vector3[point[counterRatate].getLenghtPoint()];

                for(int i = 0; i < point[counterRatate].getLenghtPoint(); i++)
                {
                    pointPath[i] = point[counterRatate].getTransform(i).position;
                    
                    if(i == point[counterRatate].getLenghtPoint()-1) counterRatate++;
                }
            }
            moveCharacter();
        }
    }

    public void moveRotate()
    {

        for(int i = 0; i < pointPath.Length; i++)
        {
            pointPath[i].y = transform.position.y;
        }

        pathRotate = transform.DOPath(pointPath, 1.2f, PathType.CatmullRom, PathMode.Full3D, 15).SetLookAt(0.1f);
    }

    public void isMoving()
    {
        isMove = true;
    }

}
