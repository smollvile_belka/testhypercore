﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class OnMouseController : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
{
    [SerializeField]
    private GameObject character;

    private Vector2 inputVector;

    private bool isMove = false;

    public virtual void OnPointerDown(PointerEventData ped)
    {
        if(isMove) OnDrag(ped);
    }

    public virtual void OnPointerUp(PointerEventData ped)
    {
        if(!isMove)
        {
            character.GetComponent<CharacterController>().moveCharacter();
            isMove = true;
        }

        inputVector.x = 0;
    }

    public virtual void OnDrag(PointerEventData ped)
    {
        Vector2 pos;

        RectTransform rect = transform.GetComponent<RectTransform>();

        if(RectTransformUtility.ScreenPointToLocalPointInRectangle( rect, ped.position, ped.pressEventCamera, out pos))
        {
            pos.x = (pos.x/rect.sizeDelta.x);

            if(pos.x < -1 ) character.GetComponent<CharacterController>().moveHorizontalCharacter(-.5f);
            else if(pos.x > 1) character.GetComponent<CharacterController>().moveHorizontalCharacter(.5f);
            else character.GetComponent<CharacterController>().moveHorizontalCharacter(0f);

            inputVector = (inputVector.magnitude > 1f) ? inputVector.normalized : inputVector;
        }
    }
    
    public float Horizontal()
    {
        if(inputVector.x != 0 ) return inputVector.x;
        else return Input.GetAxis("Horizontal");
    }

}
