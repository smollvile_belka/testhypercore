﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Point : Pointer<GameObject>
{
    public Transform getTransform(int idPoint)
    {
        return point[idPoint].transform;
    }

    public int getLenghtPoint()
    {
        return point.Length;
    }
}

public class Pointer<T>
{
    [SerializeField]
    protected GameObject[] point;
}
