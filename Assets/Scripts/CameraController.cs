﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField]
    private GameObject character;
    private Vector3 offset;
 
    private void Start() 
    {
        offset = character.transform.position - transform.position;
    }

    void LateUpdate() 
    {
        Transform tr = character.transform;
        Quaternion rotation = Quaternion.LookRotation(tr.forward,  Vector3.up);

        Vector3 vc = tr.localPosition - (rotation*offset);
        Vector3 pos = Vector3.Lerp(transform.localPosition, vc, Time.fixedDeltaTime *100f);

        transform.localPosition = pos;
        transform.LookAt(tr.transform);
    }

}